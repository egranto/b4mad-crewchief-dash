# Homeassistant part


## I'm using 

[Homeassistant](https://www.home-assistant.io/) 

[HASS Workstation Service](https://github.com/sleevezipper/hass-workstation-service) which seems i need to change 

with this service i can create sensors usin WMI Query 

so i can check if we had something open like 

```
  SELECT ProcessId FROM Win32_Process WHERE Name='CrewChiefV4.exe' \n
  SELECT ProcessId FROM Win32_Process WHERE Name='SimHubWPF.exe'
  SELECT ProcessId FROM Win32_Process WHERE Name='AC2-Win64-Shipping.exe'
  SELECT ProcessId FROM Win32_Process WHERE Name='iRacingSim64DX11.exe'
```


(this will return the processID)

other things to take in consideration my configuration.yaml looks like this 

```
binary_sensor: !include binary.yaml 
device_tracker: !include tracker.yaml 
media_player: !include players.yaml 
sensor: !include_dir_merge_list sensors/ 
template: !include_dir_merge_list templates/ 
mqtt: !include MQTT_sensors.yaml 
```

which may need to understand my sensors later 
